from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


class Survey(models.Model):
    name = models.CharField('Nome', max_length=400)
    description = models.TextField('Descrição')

    def __unicode__(self):
        return (self.name)

    def questions(self):
        if self.pk:
            return Question.objects.filter(survey=self.pk)
        else:
            return None

    def __str__(self):
        return ('%s' % self.name)

    class Meta:
        verbose_name = 'Pesquisa'
        verbose_name_plural = 'Pesquisas'


class Category(models.Model):
    name = models.CharField('Nome', max_length=400)
    survey = models.ForeignKey(Survey, on_delete=models.SET_NULL, null=True, )

    def __unicode__(self):
        return (self.name)

    def __str__(self):
        return ('%s' % self.name)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'


def validate_list(value):
    '''takes a text value and verifies that there is at least one comma '''
    values = value.split(',')
    if len(values) < 2:
        raise ValidationError(
            "The selected field requires an associated list of choices. Choices must contain more than one item.")


class Question(models.Model):
    TEXT = 'text'
    RADIO = 'radio'
    SELECT = 'select'
    SELECT_MULTIPLE = 'select-multiple'
    INTEGER = 'integer'

    QUESTION_TYPES = (
        (TEXT, 'Texto'),
        (RADIO, 'Opção'),
        (SELECT, 'Seleção'),
        (SELECT_MULTIPLE, 'Escolher vários'),
        (INTEGER, 'inteiro'),
    )

    text = models.TextField('Pergunta')
    required = models.BooleanField('Obrigatório')
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL, verbose_name='Categoria')
    survey = models.ForeignKey(Survey, null=True, on_delete=models.SET_NULL)
    question_type = models.CharField(max_length=200, choices=QUESTION_TYPES, default=TEXT,
                                     verbose_name='Tipo de resposta')
    # the choices field is only used if the question type
    choices = models.TextField(blank=True, null=True,
                               help_text='Se a questão é do tipo "Opção," "Seleção," ou "Escolher vários" de uma lista de opções separadas por vírgula.',
                               verbose_name='Opções')

    def save(self, *args, **kwargs):
        if (self.question_type == Question.RADIO or self.question_type == Question.SELECT
                or self.question_type == Question.SELECT_MULTIPLE):
            validate_list(self.choices)
        super(Question, self).save(*args, **kwargs)

    def get_choices(self):
        '''
        parse the choices field and return a tuple formatted appropriately
        for the 'choices' argument of a form widget.
        '''
        choices = self.choices.split(',')
        choices_list = []
        for c in choices:
            c = c.strip()
            choices_list.append((c, c))
        choices_tuple = tuple(choices_list)
        return choices_tuple

    def __unicode__(self):
        return (self.text)

    def __str__(self):
        return ('%s' % self.text)

    class Meta:
        verbose_name = 'Pergunta'
        verbose_name_plural = 'Perguntas'


class Response(models.Model):
    # a response object is just a collection of questions and answers with a
    # unique interview uuid
    created = models.DateTimeField('Data de criação', auto_now_add=True)
    updated = models.DateTimeField('Data de atualisação', auto_now=True)
    survey = models.ForeignKey(Survey, null=True, on_delete=models.SET_NULL, verbose_name='Pesquisa')
    interviewer = models.CharField('Entrevistado', max_length=400)
    interviewee = models.CharField('Entrevistador', max_length=400)
    conditions = models.TextField('Condições', blank=True, null=True)
    comments = models.TextField('Comentários', blank=True, null=True)
    interview_uuid = models.CharField("Código de identificação", max_length=36)

    def __unicode__(self):
        return ("response %s" % self.interview_uuid)

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class AnswerBase(models.Model):
    question = models.ForeignKey(Question, null=True, on_delete=models.SET_NULL, verbose_name='Pergunta')
    response = models.ForeignKey(Response, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


# these type-specific answer models use a text field to allow for flexible
# field sizes depending on the actual question this answer corresponds to. any
# "required" attribute will be enforced by the form.
class AnswerText(AnswerBase):
    body = models.TextField('Resposta', blank=True, null=True)

    def __str__(self):
        return ('%s' % self.body)

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class AnswerRadio(AnswerBase):
    body = models.TextField('Resposta', blank=True, null=True)

    def __str__(self):
        return ('%s' % self.body)

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class AnswerSelect(AnswerBase):
    body = models.TextField('Resposta', blank=True, null=True)

    def __str__(self):
        return ('%s' % self.body)

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class AnswerSelectMultiple(AnswerBase):
    body = models.TextField('Resposta', blank=True, null=True)

    def __str__(self):
        return ('%s' % self.body)

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class AnswerInteger(AnswerBase):
    body = models.IntegerField('Resposta', blank=True, null=True)

    def __str__(self):
        return ('%s' % self.body)

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class Client(models.Model):
    name = models.CharField('Nome', max_length=400)
    addr = models.CharField('Endereço', max_length=400)
    email = models.CharField('E-mail', max_length=400)
    telefone = models.CharField('Telefone', max_length=400)
    survey_id = models.ManyToManyField(Survey, verbose_name='Pesquisas')
    user_id = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, verbose_name='Usuário')

    def __str__(self):
        return ('%s' % self.name)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'


class Survey_Client(models.Model):
    name = models.CharField('Nome', max_length=200, null=True, default=None)
    telefone = models.CharField('Telefone', max_length=200, null=True, default=None)
    email = models.CharField('E-mail', max_length=200, null=True, default=None)
    notifications = models.CharField('Notificações', max_length=3, null=True, default=None)

    def __str__(self):
        return ('%s' % self.name)

    class Meta:
        verbose_name = 'Cliente da pesquisa'
        verbose_name_plural = 'Clientes da pesquisa'
