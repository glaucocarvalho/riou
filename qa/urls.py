from django.urls import include, re_path

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from riou import settings
from . import views

admin.autodiscover()
media_url = settings.MEDIA_URL.lstrip('/').rstrip('/')

urlpatterns = [
	re_path(r'^$', views.Index, name='home'),
	re_path(r'^survey/(?P<id>\d+)/$', views.SurveyDetail, name='survey_detail'),
	re_path(r'^confirm/(?P<uuid>\w+)/$', views.Confirm, name='confirmation'),
	re_path(r'^privacy/$', views.privacy, name='privacy_statement'),


	# Uncomment the admin/doc line below to enable admin documentation:
	# re_path(r'^admin/doc/', include('django.contrib.admindocs.urls')),
]

# media url hackery. le sigh.
# urlpatterns += [
#     (r'^%s/(?P<path>.*)$' % media_url, 'django.views.static.serve',
#      { 'document_root': settings.MEDIA_ROOT, 'show_indexes':True }),
# ]
