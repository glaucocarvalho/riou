from django.apps import AppConfig


class QaConfig(AppConfig):
    name = 'qa'
    verbose_name = 'Pesquisa de satisfação'
