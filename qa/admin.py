from .models import *
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from salmonella.admin import SalmonellaMixin


class QuestionInline(admin.TabularInline):
    model = Question
    ordering = ('category',)
    extra = 0


class CategoryInline(admin.TabularInline):
    model = Category
    extra = 0


class SurveyAdmin(admin.ModelAdmin):
    inlines = [CategoryInline, QuestionInline]


class AnswerBaseInline(admin.TabularInline):
    fields = ('question', 'body')
    readonly_fields = ('question',)
    extra = 0


class AnswerTextInline(AnswerBaseInline):
    model = AnswerText

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


class AnswerRadioInline(AnswerBaseInline):
    model = AnswerRadio

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


class AnswerSelectInline(AnswerBaseInline):
    model = AnswerSelect

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


class AnswerSelectMultipleInline(AnswerBaseInline):
    model = AnswerSelectMultiple

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


class AnswerIntegerInline(AnswerBaseInline):
    model = AnswerInteger

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


class ResponseAdmin(admin.ModelAdmin):
    # list_display = ('interview_uuid', 'interviewer', 'created', 'survey')
    list_display = ('interview_uuid', 'created', 'survey')
    inlines = [AnswerTextInline, AnswerRadioInline, AnswerSelectInline, AnswerSelectMultipleInline, AnswerIntegerInline]
    # specifies the order as well as which fields to act on
    # readonly_fields = ('survey', 'created', 'updated', 'interview_uuid')

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


# admin.site.register(Question, QuestionInline)
# admin.site.register(Category, CategoryInline)
admin.site.register(Survey, SurveyAdmin)
admin.site.register(Response, ResponseAdmin)


class QuestionAdmin(admin.ModelAdmin):
    model = Question


class CategoryAdmin(admin.ModelAdmin):
    model = Category


class AnswerBaseAdmin(admin.ModelAdmin):
    list_display = ('question', 'response', 'created', 'updated')
    model = AnswerBase


class ClientAdmin(SalmonellaMixin, admin.ModelAdmin):
    model = Client
    salmonella_fields = ('survey_id', 'user_id')


class ClientQuestionInline(admin.TabularInline):
    model = Client.survey_id.through
    extra = 0


admin.site.register(Client, ClientAdmin)


class Survey_ClientAdmin(admin.ModelAdmin):
    model = Survey_Client
    list_display = ('name', 'telefone', 'email', 'Notificações')

    def Notificações(self, obj):
        ret = False
        if obj.notifications == 'sim':
            ret = True
        return ret
    Notificações.boolean = True

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False


admin.site.register(Survey_Client, Survey_ClientAdmin)


# class AnswerBaseAdmin(admin.ModelAdmin):
#     model = AnswerBase
#
#     def get_readonly_fields(self, request, obj=None):
#         return [f.name for f in self.model._meta.fields]
#
#     def has_add_permission(self, request):
#         return False
#
#
# admin.site.register(AnswerBase, AnswerBaseAdmin)
