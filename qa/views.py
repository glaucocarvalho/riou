from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
# from django.shortcuts import render_to_response
# from django.template import RequestContext
# from django.core import urlresolvers
# from django.contrib import messages
import datetime
# import settings
import pprint
from django.contrib.auth.decorators import login_required

from .models import Question, Survey, Category, Client
from .forms import ResponseForm


@login_required
def Index(request):
    user_name = request.user
    client = Client.objects.get(user_id=request.user.id)
    return render(request, str(user_name)+'.html', {
                                                'client': client,
                                                })


@login_required
def SurveyDetail(request, id):
    client = Client.objects.get(id=id)
    form_list = []
    for survey in client.survey_id.all():
        # category_items = Category.objects.filter(survey=survey)
        # categories = [c.name for c in category_items]
        # print ('categories for this survey:')
        # print (categories)
        # pprint.pprint(client.survey_id.all())
        if request.method == 'POST':
            # pprint.pprint(request.POST)
            form = ResponseForm(request.POST, survey=survey)
            if form.is_valid():
                response = form.save()
        else:
            form = ResponseForm(survey=survey)
        form_list.append([survey.name, form])
    # TODO sort by category
    if request.method == 'POST':
        return HttpResponseRedirect("/confirm/%s" % response.interview_uuid)
    else:
        return render(request, 'survey.html', {
                                        'client': id,
                                        'client_name': client.name,
                                        'survey': survey,
                                        'form_list': form_list,
                                        })


@login_required
def Confirm(request, uuid):
    # TODO configurar e-mail
    email = 'email@email.com'
    return render(request, 'confirm.html', {'uuid': uuid, "email": email})


@login_required
def privacy(request):
    return render(request, 'privacy.html')
